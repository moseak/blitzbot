const builder = require('botbuilder');
const restify = require('restify');
const request = require('request');
var fs = require('fs');

var team_convert = {"FNC":"Fnatic","OG":"Origen","GIA":"Giants","VIT":"Vitality","G2":"Gamers2","TSM":"Team SoloMid","TL":"Team Liquid","P1":"Phoenix1","IMT":"Immortals","FLY":"Flyquest","NV":"EnVyUs","FOX":"Echo Fox","DIG":"Dignitas","CLG":"Counter Logic Gaming","C9":"Cloud9","UOL":"Unicorns of Love","SPY":"Splyce","ROC":"Roccat","MSF":"Misfits","H2K":"H2K","FNA":"Fnatic Academy","PSG":"Paris St Germain","MFA":"Misfit Academy","S04":"FC SCHALKE 04","KGN":"Team Kinguin","MIL":"Millenium","EUN":"Eunited","GCU":"Gold Gods Jackals","TS":"Tempo Storm","DFX":"Delta Fox","TG":"Team Gates"};
var month = {"janvier":"01","février":"02","mars":"03","avril":"04","mai":"05","juin":"06","juillet":"07","août":"08","septembre":"09","octobre":"10","novembre":"11","decembre":"12"};
var retour = false;


const server = restify.createServer();
server.listen(3978, () => {
   console.log('Listening on port 3978'); 
});

const connector = new builder.ChatConnector({
    appId: '3719d4bd-344b-4d42-999a-a425ccefb116',
    appPassword: 'LX6hscenJEnbcQcKnDj0pmo'
});
const bot = new builder.UniversalBot(connector);
server.post('/', connector.listen());


class ApiAiRecognizer {
  constructor(token) { this._token = token; }

  recognize(context, done) {
    const opts = {
      url: 'https://api.api.ai/v1/query?v=20150910',
      headers: { Authorization: `Bearer ${this._token}` },
      json: {
        query: context.message.text,
        lang: 'fr',
        sessionId: '0000',
      },
    };

    request.post(opts, (err, res, body) => {
      if (err) return done(err);

      return done(null, {
        score: body.result.score,
        intent: body.result.metadata.intentName,
		response: body.result.fulfillment.speech,
        entities: body.result.parameters,
      });
    });
  }
}

const recognizer = new   ApiAiRecognizer('a069a9307a0f47e185486fec95e29b0c');

const intents = new builder.IntentDialog({
  recognizers: [recognizer],
});

bot.dialog('/',intents);

intents.matches('get_team_from_player', [
  (session, args) => {
	const BD_BlitzBot_playerstats = require('./BD_BlitzBot_playerstats.json');
	var player = args.entities.player_name;
	var results = [];
	var ok = false;
	for (var i=0 ; i < BD_BlitzBot_playerstats.length ; i++)
	{
		if (BD_BlitzBot_playerstats[i]["player"] == player) {
			results.push(BD_BlitzBot_playerstats[i]["team"]);
			ok = true;
		}
	}
	
	if(ok)
	{
		session.send("le joueur "+ player + " est dans l'equipe " + results);
	}else
	{
		session.send("Désolé, je n'ai pas trouvé le joueur que tu me demande.");
	}
	

  }
]);

intents.matches('get_link', [
	(session) => {
		session.send(
			'Tu peux aller là si tu veux voir les prochains matchs : https://www.twitch.tv/lolesports'
		);
	}
]);

intents.matches('get_help', [
	(session) => {
	
		var newUser = initUser(session);
		session.send(
			"Bonjour " + session.message.user.name+ "\n\r" +  newUser + "Je suis BlitzBot \n\r Plusieurs options son disponible : \n\r *Parier\n\r*recuperer des données sur un joueur\n\r * recuperer des données sur une équipe\n\r * Récupérer les prochains match\n\r * Récupérer vos données\n\r * Récupérer vos paris"		
		);
		
	}
]);

intents.matches('set_bet', [
	(session, args) => {
		var reponse = "";
		var ok = true;
		var newUser = initUser(session);
		const BD_BlitzBot_futurmatch = require('./BD_BlitzBot_futurmatchs.json');
		const BD_BlitzBot_bets = require('./BD_BlitzBot_bets.json');
		const BD_BlitzBot_users = require('./BD_BlitzBot_users.json');
		if(args.entities.number == "")
		{
			reponse += "Il manque la somme à parier.\n\r";
			ok = false;
		}
		if(args.entities.team_name == "")
		{
			reponse += "Il manque la team sur laquelle parier.\n\r";
			ok = false;
		}
		if(args.entities.bet_result == "")
		{
			reponse += "Indiquer la victoire ou la défaite de l'équipe.\n\r";
			ok = false;
		}
		if(args.entities.date == "")
		{
			reponse += "Il manque la date du match sur lequel vous voulez pariez.\n\r";
			ok = false;
		}
		if(ok)
		{
			// verif de l'existence du match
			var j = 0;
			while ( j < BD_BlitzBot_futurmatch.length && ((BD_BlitzBot_futurmatch[j]["blueside"] != args.entities.team_name && BD_BlitzBot_futurmatch[j]["redside"] != args.entities.team_name) || args.entities.date != convert_date(BD_BlitzBot_futurmatch[j]["date"])))
			{
				j++;	
			}
			if(j == BD_BlitzBot_futurmatch.length)
			{
				reponse += "Le match que tu demande n'existe pas.";
				ok = false;
			}
			
			if(ok)
			{
				// verif si l'utilisateur à déja parié sur ce match
				var idMatch = args.entities.date + "-" + BD_BlitzBot_futurmatch[j]["blueside"] + "-" + BD_BlitzBot_futurmatch[j]["redside"];
				console.log(idMatch);
				var k = 0;
				while ( k < BD_BlitzBot_bets.BETS_DATA.length && (idMatch != BD_BlitzBot_bets.BETS_DATA[k]["ID_MATCH"] || session.message.user.name != BD_BlitzBot_bets.BETS_DATA[k]["USER_NAME"]))
				{
					k++;	
				}
				if(k != BD_BlitzBot_bets.BETS_DATA.length)
				{
					reponse += "Tu as deja parié sur ce match.";
					ok = false;
				}
				
				if(ok)
				{
					// verif du solde du compte
					var solde = getSolde(session.message.user.name);
					if(solde < args.entities.number)
					{
						reponse += "Désolé, tu n'a pas assez de point pour faire ce pari.\n\r Tu n'a que " + solde + " points.";
						ok = false;
					}
					
					if(ok)
					{
						// ecriture du pari
						console.log("JE SUIS là");
						BD_BlitzBot_bets.BETS_DATA.push(
						{
							USER_NAME : session.message.user.name,
							VALUE : args.entities.number,
							CHOICE : args.entities.bet_result,
							TEAM_NAME : args.entities.team_name,
							ID_MATCH : idMatch,
							RETREIVE : 0
						});
						fs.writeFile('BD_BlitzBot_bets.json', JSON.stringify(BD_BlitzBot_bets), 'utf8', callback);

						// retrait de la somme pariée
						i = 0;
						while (BD_BlitzBot_users.USERS_DATA[i].NAME != session.message.user.name )
						{
							i++;	
						}
						BD_BlitzBot_users.USERS_DATA[i].SOLDE -= args.entities.number;
						fs.writeFile('BD_BlitzBot_users.json', JSON.stringify(BD_BlitzBot_users), 'utf8', callback);

						if(retour)
						{
							reponse += "Félicitation ! Ton pari a bien été pris en compte.\n\r";
							
							retour = false;
						}
						else
						{
							reponse += "Désolé, je n'ai pas réussi à enregistrer ton pari.";
						}
					}
				}
			}
			
		}
		
		session.send(reponse);
	}
]);


// NE FONCTIONNE PAS 
intents.matches('get_bet', [
	(session, args) => {
		const BD_BlitzBot_bets = require('./BD_BlitzBot_bets.json');
		const BD_BlitzBot_users = require('./BD_BlitzBot_users.json');
		const BD_BlitzBot_pastmatchs = require('./BD_BlitzBot_pastmatchs.json');
		var reponse = "";
		var somme = 0;
		
		var aucun_pari = true;
	
		for(var i = 0; i < BD_BlitzBot_bets.length; i ++)
		{
			for(var j = 0; j < BD_BlitzBot_pastmatchs.length; j++)
			{
				var idMatch = args.entities.date + "-" + BD_BlitzBot_pastmatchs[j]["blueside"] + "-" + BD_BlitzBot_pastmatchs[j]["redside"];
				
				if(BD_BlitzBot_bets.BETS_DATA[i].USER_NAME == session.message.user.name && BD_BlitzBot_bets.BETS_DATA[i].ID_MATCH == idMatch && BD_BlitzBot_bets.BETS_DATA[i].RETREIVE == 0)
				{
					aucun_pari = false;
					if(pari_gagne(BD_BlitzBot_bets[i],BD_BlitzBot_pastmatchs[j]))
					{
						for(var k=0;k < BD_BlitzBot_bets.length;k++)
						{
							if(BD_BlitzBot_bets[k].ID_MATCH == BD_BlitzBot_bets[i].ID_MATCH)
							{
								somme += BD_BlitzBot_bets[k].VALUE;
							}
						}
						
						var k = 0;
						while (BD_BlitzBot_users.USERS_DATA[k].NAME != session.message.user.name )
						{
							k++;	
						}
						BD_BlitzBot_users.USERS_DATA[k].SOLDE += somme;
						BD_BlitzBot_bets.BETS_DATA[i].RETREIVE = 1;
						
					}else
					{
						BD_BlitzBot_bets.BETS_DATA[i].RETREIVE = 1;		
					}
					
				}
			}
		}
		
		if(aucun_pari)
		{
			reponse += "Tu n'a aucun pari à récupérer.";
		}else
		{
			reponse += "j'ai récupéré tout vos pari fini et vous avez gagné " + somme + " point(s).";
			fs.writeFile('BD_BlitzBot_users.json', JSON.stringify(BD_BlitzBot_users), 'utf8', callback);
			fs.writeFile('BD_BlitzBot_bets.json', JSON.stringify(BD_BlitzBot_bets), 'utf8', callback);
		}
		session.send(
			reponse
		);
	}
]);


intents.matches('get_user_data', [
	(session) => {
		var newUser = initUser(session);
		
		if(newUser == "")
		{	
			session.send("Vous êtes " + session.message.user.name + ". Votre solde est de " + getSolde(session.message.user.name) + " points.");
		}
		else
		{
			session.send(newUser);
		}
		

	}
]);

intents.matches('get_stats_team', [
	(session, args) => {
		const BD_BlitzBot_teamsstats = require('./BD_BlitzBot_teamsstats.json');
		var team_name = args.entities.team_name;
		var extended_team = team_convert[team_name];
		var reponse = "";
		var notfound = true;
		
		for(var i=0; i < BD_BlitzBot_teamsstats.length; i++)
		{
			if(BD_BlitzBot_teamsstats[i]["teamname"] == extended_team)
			{
				notfound = false;
				if(BD_BlitzBot_teamsstats[i]["games"] != 0)
				{
					reponse += "La team " + extended_team + " a gagné " + BD_BlitzBot_teamsstats[i]["wins"] + " fois et perdu " + BD_BlitzBot_teamsstats[i]["loses"] + " fois pour un total de " + BD_BlitzBot_teamsstats[i]["games"] + " partie(s).";
				}else
				{
					reponse += "La team " + extended_team + " n'a toujours pas joué ce mois ci.";
				}
			}
		}
		
		if(notfound)
		{
			reponse += "Je n'ai pas trouvé d'info sur la team " + extended_team + ".";
		}
		
		session.send(
			reponse
		);
	}
]);

intents.matches('get_stats_player', [
	(session, args) => {
		const BD_BlitzBot_playerstats = require('./BD_BlitzBot_playerstats.json');
		var player_name = args.entities.player_name;
		//var extended_team = team_convert[team_name];
		var reponse = "";
		var notfound = true;
		
		for(var i=0; i < BD_BlitzBot_playerstats.length; i++)
		{
			if(BD_BlitzBot_playerstats[i]["player"] == player_name)
			{
				notfound = false;
				if(BD_BlitzBot_playerstats[i]["gamesplayed"] != 0)
				{
					reponse += "Le joueur " + args.entities.player_name + " joue en tant que " + BD_BlitzBot_playerstats[i]["pos"] + " chez " + BD_BlitzBot_playerstats[i]["team"] + ".\n\r Il possède un kda de " + BD_BlitzBot_playerstats[i]["kda"] + ", et un winrate de " + BD_BlitzBot_playerstats[i]["winrate"] + ".\n\r Il a participé à hauteur de " + BD_BlitzBot_playerstats[i]["kp"] + " aux éléminations de sa team.\n\r Il a joué un total de " +  BD_BlitzBot_playerstats[i]["gamesplayed"] + " partie(s).";
				}else
				{
					reponse += "Le joueur " + args.entities.player_name + " n'a toujours pas joué.";
				}
			}
		}
		
		if(notfound)
		{
			reponse += "Je n'ai pas trouvé d'info sur le joueur " + player_name + ".";
		}
		
		session.send(
			reponse
		);
	}
]);

intents.matches('get_next_match', [
	(session, args) => {
		const BD_BlitzBot_futurmatchs = require('./BD_BlitzBot_futurmatchs.json');
		var reponse = "";
		var temp = "";
		var tot = 0;
		if(args.entities.team_name != "")
		{
			for(var i = 0; i < BD_BlitzBot_futurmatchs.length; i++)
			{
				if(BD_BlitzBot_futurmatchs[i]["redside"] == args.entities.team_name || BD_BlitzBot_futurmatchs[i]["blueside"] == args.entities.team_name)
				{
						tot++;
						temp += team_convert[BD_BlitzBot_futurmatchs[i]["redside"]] + "(redside) versus " + team_convert[BD_BlitzBot_futurmatchs[i]["blueside"]] + "(blueside) le " + BD_BlitzBot_futurmatchs[i]["date"] + " à " + BD_BlitzBot_futurmatchs[i]["heure"] + ".\n\r";
				}
			}
			
			if(tot == 0)
			{
				reponse += "La team " + team_convert[args.entities.team_name] + " n'a pour l'instant aucun match à venir.";
			}else
			{
				reponse += "Il y a " + tot + " match(s) à venir pour la team " + team_convert[args.entities.team_name] + " : \n\r" + temp;
			}	
		}else
		{
			reponse += "Il y a " + BD_BlitzBot_futurmatchs.length + " match(s) à venir : \n\r";
			for(var i = 0; i < BD_BlitzBot_futurmatchs.length; i++)
			{
				reponse += team_convert[BD_BlitzBot_futurmatchs[i]["redside"]] + "(redside) versus " + team_convert[BD_BlitzBot_futurmatchs[i]["blueside"]] + "(blueside) le " + BD_BlitzBot_futurmatchs[i]["date"] + " à " + BD_BlitzBot_futurmatchs[i]["heure"] + ".\n\r";
			}
			
		}
		session.send( reponse);
	}
]);



intents.onDefault([
	(session, args) => {
		session.send(
			args.response
		);
	}
]);

var callback = function(){
	console.log('testcall' + retour);
	retour = true;
	
};

var initUser = function(session){
	    var i = 0;
		var newUser = "";
		BD_BlitzBot_users = require('./BD_BlitzBot_users.json');
		while ( i <BD_BlitzBot_users.USERS_DATA.length && BD_BlitzBot_users.USERS_DATA[i].NAME != session.message.user.name )
		{
				i++;	
		}
		if(i==BD_BlitzBot_users.USERS_DATA.length)
		{
			
			BD_BlitzBot_users.USERS_DATA.push(
			{
				NAME : session.message.user.name, SOLDE : 100
			});
			fs.writeFile('BD_BlitzBot_users.json', JSON.stringify(BD_BlitzBot_users), 'utf8', callback);
			newUser = "Vous etes un nouveau utilisateur. \n\r Votre solde a été crédité de 100 points. \n\r"
		
		}
		
		return newUser;
		
		
	};
	
var getSolde = function(userName){
	var BD_BlitzBot_users = require('./BD_BlitzBot_users.json');
	
	for (var i=0 ; i < BD_BlitzBot_users.USERS_DATA.length ; i++)
	{
		if (BD_BlitzBot_users.USERS_DATA[i]["NAME"] == userName) {
			return BD_BlitzBot_users.USERS_DATA[i]["SOLDE"];
		}
	}
}

// function wait(ms){
   // var start = new Date().getTime();
   // var end = start;
   // while(end < start + ms) {
     // end = new Date().getTime();
  // }
// }

function convert_date(date)
{				 
				 
	var res = date.split(" ");
	
	if(res[1] <= 9)
	{
		res[1] = "0"+res[1];
	}
	
	var year = new Date().getFullYear();
	
	var result = year + "-" + month[res[2]] + "-" + res[1];
	
	return result;
}

//NE FONCTIONNE PAS --- FONCTION NON FINI
function pari_gagne(pari,match)
{
	var choice = pari.CHOICE;
	var choice_team = pari.TEAM_NAME;
	
	var red_team = team_convert[match["redside"]];
	var blue_team = team_convert[match["blueside"]];
	var red_score = match["redside_score"];
	var blue_score = match["blueside_score"];
	
	var result = red_score - blue_score;
	
	if(choice == "win")
	{
		
		
	}else
	{
		
	}
	
	
	
	
}


