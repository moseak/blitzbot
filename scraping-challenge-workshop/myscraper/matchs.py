# -*- coding: utf-8 -*-

from scrapy import Item, Field


class MatchItem(Item):

	url = Field()

	date = Field()
	heure = Field()
	blueside = Field()
	blueside_score = Field()
	redside = Field()
	redside_score = Field()