# -*- coding: utf-8 -*-

from scrapy import Item, Field


class TeamItem(Item):

	url = Field()

	teamname = Field()
	games = Field()
	wins = Field()
	loses = Field()