# -*- coding: utf-8 -*-

from scrapy import Spider, Request
from ..items import PersonItem


class MyScraper(Spider):
    name = u'myscraper'


    def start_requests(self):
        # First request
        yield Request(
            url=u'http://oracleselixir.com/statistics/na/na-lcs-2017-spring-regular-season-player-statistics/',
            callback=self.parse,
        )


    def parse(self, response):
        # Find a list of div which contains a person (use CSS)
        persons_el = response.css('.tablepress-id-258 tbody tr')

        # Browse the list
        for person_el in persons_el:

            # Create a new item
            item = PersonItem()

            # Extract the name of the person (use CSS)
            item['player'] = person_el.css('.column-1::text').extract_first()

            # Extract the ticket fare of the person (use CSS)
            item['team'] = person_el.css('.column-2::text').extract_first()

            item['pos'] = person_el.css('.column-3::text').extract_first()

            item['gamesplayed'] = person_el.css('.column-4::text').extract_first()

            item['winrate'] = person_el.css('.column-5::text').extract_first()

            item['kills'] = person_el.css('.column-6::text').extract_first()

            item['deaths'] = person_el.css('.column-7::text').extract_first()

            item['assists'] = person_el.css('.column-8::text').extract_first()

            item['kda'] = person_el.css('.column-9::text').extract_first()

            item['kp'] = person_el.css('.column-10::text').extract_first()

            # Export the item
            yield item
