# -*- coding: utf-8 -*-

from scrapy import Spider, Request, Selector
from ..teams import TeamItem

import json

class MyScraper4(Spider):
    name = u'myscraper4'


    def start_requests(self):
        # First request
        yield Request(
            url=u'http://oracleselixir.com/statistics/eu/eu-lcs-2017-spring-regular-season-team-statistics/',
            callback=self.parse,
        )


    def parse(self, response):
        # Find a list of div which contains a person (use CSS)
        teams_el = response.css('.tablepress-id-255 tbody tr')

        for team_el in teams_el:
            item = TeamItem()
            item['teamname'] = team_el.css('.column-1::text').extract_first()
            item['games'] = team_el.css('.column-2::text').extract_first()
            item['wins'] = team_el.css('.column-3::text').extract_first()
            item['loses'] = team_el.css('.column-4::text').extract_first()

            yield item
        
            
