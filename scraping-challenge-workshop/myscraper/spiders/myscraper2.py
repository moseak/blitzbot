# -*- coding: utf-8 -*-

from scrapy import Spider, Request, Selector
from ..matchs import MatchItem

import json

class MyScraper2(Spider):
    name = u'myscraper2'


    def start_requests(self):
        # First request
        yield Request(
            url=u'http://eu.lolesports.com/fr/api/widget/schedule?timezone=Europe%2FParis&leagues=26&leagues=3&leagues=2&leagues=6&leagues=7&leagues=8&leagues=5&leagues=4&leagues=9&leagues=10&leagues=1&leagues=12&slug=eu-lcs&tab=results',
            callback=self.parse,
        )


    def parse(self, response):

        raw = response.text
        raw_json = json.loads(raw)
        raw_html = raw_json['resultsHtml']

        content = Selector(text=raw_html)

        dates = content.css('.schedule__row-group')

        for date in dates:
                date_raw = date.css('.schedule__row--date h2::text').extract_first()
                lignes = date.css('.schedule__table-row')
                for match in lignes:
                    item = MatchItem()
                    item['date'] = date_raw
                    item['heure'] = match.css('.time:last-child::text').extract_first()

                    item['blueside'] = match.css('.team a::text').extract_first()
                    item['blueside_score'] = match.css('.score::text').extract_first()

                    item['redside'] = match.css('.team:nth-child(1) a::text').extract()[1]
                    item['redside_score'] = match.css('.score:last-child::text').extract_first()

                    yield item
        
            
