# -*- coding: utf-8 -*-

from scrapy import Item, Field


class PersonItem(Item):

	url = Field()

	player = Field()
	team = Field()
	pos = Field()
	gamesplayed = Field()
	winrate = Field()
	kills = Field()
	deaths = Field()
	assists = Field()
	kda = Field()
	kp = Field()