# Scraping for BlitzBot

### Step 1: Fill prerequisite

#### Install Python 2.7

Scrapy works only with Python 2.7.

[Please install Python 2.7, and not Python 3.x!](https://www.python.org/downloads/release/python-2710)

#### Install dependencies

On Ubuntu 16:

```
sudo apt-get install python-dev python-pip libssl-dev libxml2-dev libxslt1-dev libffi-dev
```

On Windows:

Download and install [Anaconda Distribution](https://www.continuum.io/downloads) for Python **2.7**.


On Mac OS X:

```
brew install python
```


#### Install Scrapy, Scrapoxy and ScrapingHub tools

```
sudo pip install scrapy scrapoxy shub
```


### Step 4: Start the scraper

```
./execute_scraper.sh
```

## Licence

See the [Licence](LICENCE.txt).
